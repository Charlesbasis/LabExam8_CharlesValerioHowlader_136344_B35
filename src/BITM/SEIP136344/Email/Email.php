<?php


namespace App\Email;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class Email extends DB{

    public $id="";

    public $name="";

    public $email="";



    public function __construct(){

        parent::__construct();

    }

//    public function index(){
//        echo "Email found!";
//    }
    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){
            $this->id = $postVariabledata['id'];
        }

        if(array_key_exists('name',$postVariabledata)){
            $this->name = $postVariabledata['name'];
        }

        if(array_key_exists('email',$postVariabledata)){
            $this->email = $postVariabledata['email'];
        }
    }

    public function store(){

        $arrData = array($this->name, $this->email);

        $sql = "Insert INTO email(name, email) VALUES(?,?)";


        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully! :)");
        else
            Message::message("Failed! Data Has Not Been Inserted! :(");

        Utility::redirect('create.php');

    } //end of store method

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from email where is_deleted = 'No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from email where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of index();

    public function update(){

        $arrData= array($this->name,$this->email);
        $sql="UPDATE email SET name = ?, email = ? WHERE id =".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    } //end of update()

    public function delete(){
        $sql= "DELETE FROM email WHERE id =".$this->id;
        $STH= $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    } // end of delete(), permanent delete


    public function trash(){

        $sql = "Update email SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from email where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed()





    public function recover(){

        $sql = "Update email SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover()




}


