<?php
require_once("../../../vendor/autoload.php");

use App\Birthday\Birthday;

$objBirthday = new Birthday();

$objBirthday->setData($_GET);
$oneData = $objBirthday->view("obj");

echo "ID: ".$oneData->id."<br>";
echo "Name: ".$oneData->name."<br>";
echo "Birthday: ".$oneData->birthday."<br>";