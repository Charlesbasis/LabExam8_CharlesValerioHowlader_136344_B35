<?php
require_once("../../../vendor/autoload.php");
use App\SummaryOfOrganization\SummaryOfOrganization;

$objSummaryOfOrganization = new SummaryOfOrganization();
$objSummaryOfOrganization->setData($_GET);
$objSummaryOfOrganization->delete();