<?php
require_once("../../../vendor/autoload.php");

use App\City\City;

$objCity = new City();

$objCity->setData($_GET);
$oneData = $objCity->view("obj");

echo "ID: ".$oneData->id."<br>";
echo "Name: ".$oneData->name."<br>";
echo "City: ".$oneData->city."<br>";

?>