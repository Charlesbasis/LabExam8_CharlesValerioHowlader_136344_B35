<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset($_SESSION)) session_start();

use App\Email\Email;

$objEmail = new Email();
$objEmail->setData($_GET);
$oneData = $objEmail->view("obj");

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Login Form Template</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--<script type= "text/javascript" src = "../../../resource/assets/js/countries.js"></script>-->
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../resource/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../resource/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>

<!-- Top content -->
<div class="top-content">

    <!--    <div class="inner-bg">-->
    <div class="container">
        <div class="row">
            <div id="amessage"> <?php echo Message::message(); ?> </div>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Edit Email</h3>
                        <p>Edit your name and email:</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-mail"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="update.php" method="post" class="login-form">
                        <input type="hidden" name="id" value="<?php echo $oneData->id; ?>">
                        <div class="form-group">
                            <label class="sr-only" for="form-name">name</label>
                            <input type="text" name="name" value="<?php echo $oneData->name; ?>" class="form-name form-control" id="form-name">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-email">email</label>
                            <input type="email" name="email" value="<?php echo $oneData->email; ?>" class="form-email form-control" id="form-email">



                        </div>
                        <button type="submit" class="btn">Edit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 social-login">
                <h3>...or login with:</h3>
                <!--                    <div class="social-login-buttons">-->
                <!--                        <a class="btn btn-link-2" href="#">-->
                <!--                            <i class="fa fa-facebook"></i> Facebook-->
                <!--                        </a>-->
                <!--                        <a class="btn btn-link-2" href="#">-->
                <!--                            <i class="fa fa-twitter"></i> Twitter-->
                <!--                        </a>-->
                <!--                        <a class="btn btn-link-2" href="#">-->
                <!--                            <i class="fa fa-google-plus"></i> Google Plus-->
                <!--                        </a>-->
                <!--                    </div>-->
            </div>
        </div>
    </div>
</div>

</div>


<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/js/scripts.js"></script>

<script type="text/javascript">

    $(function(){
        setTimeout(function(){
            $("#amessage").fadeOut('Slow')
        }, 2000);
    });

</script>

<!--[if lt IE 10]>
<script src="../../../resource/js/placeholder.js"></script>
<![endif]-->

</body>

</html>