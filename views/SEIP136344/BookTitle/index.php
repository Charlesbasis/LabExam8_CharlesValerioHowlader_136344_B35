<!DOCTYPE html>
<html lang="en">

<head>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
<!--    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">-->
    <script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>

</head>

<?php
require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;


$obj_BookTitle= new BookTitle();

$allData = $obj_BookTitle->index("obj");

$serial=1;

echo "<table border='2px'>";

echo "<th> Serial </th><th> ID </th> <th> Book Title </th> <th> Author Name </th> <th> Action </th>";

foreach($allData as $oneData){

    echo "<tr style='height: 40px'>";
    echo "<td> $serial </td>";
    echo "<td> $oneData->id </td>";
    echo "<td> $oneData->book_title </td>";
    echo "<td> $oneData->author </td>";

    echo "<td>
<a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a>
<a href='edit.php?id=$oneData->id'><button class='btn btn-info'>Edit</button></a>
<a href='trash.php?id=$oneData->id'><button class='btn btn-danger'>Trash</button></a>
<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a>

    </td>";

   echo "</tr>";
    $serial++;
} //end of foreach loop

echo "</table>";
